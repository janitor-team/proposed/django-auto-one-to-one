django-auto-one-to-one (3.3.5-1) unstable; urgency=medium

  * New upstream version 3.3.5
  * d/control: Updated Standards-Version from 4.6.0 to 4.6.2.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 13 Feb 2023 18:47:31 +0100

django-auto-one-to-one (3.3.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Petter Reinholdtsen ]
  * d/control: Updated Standards-Version from 4.5.0 to 4.6.0.
  * d/control: Updated from debhelper level 12 to 13.
  * d/gbp.conf: Reinsert pristine-tar=True which seem to have been
    removed by mistake in fcceadd32ded401a0bc4ce30e25c7585d5a8a696.
  * Updated to new upstream version 3.3.1.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 19 Sep 2021 10:13:57 +0200

django-auto-one-to-one (3.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.4.1.
  * Specify "Rules-Requires-Root: no".

 -- Chris Lamb <lamby@debian.org>  Tue, 15 Oct 2019 12:10:54 -0700

django-auto-one-to-one (3.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Move to debhelper compat level 12.
  * Bump Standards-Version to 4.4.0.
  * Avoid dh_make/uscan(1) usage of "<project>" in debian/watch.

 -- Chris Lamb <lamby@debian.org>  Wed, 11 Sep 2019 10:40:39 +0100

django-auto-one-to-one (3.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Wed, 03 Oct 2018 12:53:07 +0100

django-auto-one-to-one (3.1.0-1) unstable; urgency=medium

  * New upstream version.
    - Update debian/copyright.
  * Enable testsuite and tidy debian/rules.
  * Use debhelper-compat virtual build-dependency.

 -- Chris Lamb <lamby@debian.org>  Tue, 25 Sep 2018 21:45:41 +0100

django-auto-one-to-one (3.0.0-1) unstable; urgency=medium

  [ Petter Reinholdtsen ]
  * Initial release. (Closes: #908723)
  * Add Chris as uploader.

  [ Chris Lamb ]
  * debian/control:
    - Set Maintainer to DPMT and move Petter to Uploaders.
    - Set Standards-Version to 4.2.1 and debhelper compat level to 11.
    - Update Vcs-{Git,Browser}
    - Improve long description.
    - Use my Debian email address.
    - wrap-and-sort -sa.
    - Drop Python 2.x build-dependencies.
    - Use autopkgtest-pkg-python as a Testsuite header.
  * Add/update myself in debian/copyright.
  * Drop boilerplate from debian/rules.
  * Drop dh_make boilerplate from debian/watch.
  * Update debian/gbp.conf to reflect DPMT repository layout/policy.
  * Don't complain about debian-watch-does-not-check-gpg-signature.

 -- Chris Lamb <lamby@debian.org>  Mon, 24 Sep 2018 19:37:55 +0100
